package filmography.dao;

import java.util.List;

import filmography.model.Film;

public interface FilmDao {
    List<Film> allFilms();
    void add(Film film);
    void delete(Film film);
    void edit(Film film);
    Film getById(int id);
}
