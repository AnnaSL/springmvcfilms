package filmography.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import filmography.dao.FilmDao;
import filmography.dao.FilmDaoImpl;
import filmography.model.Film;

@Service
public class FilmServiceImpl implements FilmService {

    private FilmDao filmDAO;

    @Autowired
    public void setFilmDAO(FilmDao filmDAO) {
        this.filmDAO = filmDAO;
    }

    @Transactional
    public List<Film> allFilms() {
        return filmDAO.allFilms();
    }

    @Transactional
    public void add(Film film) {
        filmDAO.add(film);
    }

    @Transactional
    public void delete(Film film) {
        filmDAO.delete(film);
    }

    @Transactional
    public void edit(Film film) {
        filmDAO.edit(film);
    }

    @Transactional
    public Film getById(int id) {
        return filmDAO.getById(id);
    }
}
